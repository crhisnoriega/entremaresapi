const AWS = require("aws-sdk");
const fs = require("fs");
const { Image } = require("../persistence/db");

const s3 = new AWS.S3({
  accessKeyId: "AKIAQKJLY56HTF77Z3EK",
  secretAccessKey: "j01jB6ab1kYtxYCPOUwZvUjfHWkhMsIGquW2Y94h"
});



const uploadS3Base64Async = async(base64, bucketKey, propertyid) => {
  const base64Data = new Buffer.from(base64.replace(/^data:image\/\w+;base64,/, ""), 'base64');

  const params = {
    Bucket: "entremares-img",
    Key: propertyid + "/" + bucketKey,
    Body: base64Data,
    ACL: "public-read"
  };

  s3.upload(params, function(s3Err, data) {
    if (s3Err) {
      console.log(s3Err);
    }
    else {
      console.log("file uploaded: " + data.Location);

      var image = new Image();

      image.url = data.Location;
      image.propertyid = propertyid;

      image.save();
    }
  });
};

const uploadS3FileSync = (fileName, bucketKey, property) => {
  var data = fs.readFileSync(fileName);

  const params = {
    Bucket: "entremares-img",
    Key: bucketKey,
    Body: data,
    ACL: "public-read"
  };

  s3.upload(params, function(s3Err, data) {
    if (s3Err) {
      console.log(s3Err);
    }
    else {
      console.log("file uploaded: " + data.Location);
      property.imageUrl = data.Location;
      property.save();

      var image = new Image();

      image.url = data.Location;
      image.productid = property.id;

      image.save();
    }
  });
};

module.exports = { uploadS3FileSync, uploadS3Base64Async };
