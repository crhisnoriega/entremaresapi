module.exports = function(Sequelize, sequelize, User) {
  class StockItem extends Sequelize.Model {}
  StockItem.init(
    {
      productId: Sequelize.STRING,
      name: Sequelize.STRING,
      unit: Sequelize.STRING,
      unitPrice: Sequelize.DOUBLE,
      quantity: Sequelize.DOUBLE
    },
    { sequelize, modelName: "stockitem" }
  );

  User.hasMany(StockItem);

  return StockItem;
};
