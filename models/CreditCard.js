module.exports = function(Sequelize, sequelize, Product) {
  class CreditCard extends Sequelize.Model {}
  CreditCard.init(
    {
      id: {
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV1
      },
      cardType: {
        type: Sequelize.ENUM,
        values: ["default", "firstone", "secondone"]
      },
      cardNumber: Sequelize.STRING,
      holder: Sequelize.STRING,
      expirationDate: Sequelize.STRING,
      securityCode: Sequelize.STRING,
      brand: Sequelize.STRING
    },
    { sequelize, modelName: "creditcard" }
  );

  return CreditCard;
};
