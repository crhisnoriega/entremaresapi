module.exports = function(Sequelize, sequelize) {
  class Reservation extends Sequelize.Model {}
  Reservation.init(
    {
      id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      codigo: Sequelize.STRING,
      customer_code: Sequelize.STRING,
      property_code: Sequelize.STRING,
      reservation_date: Sequelize.STRING,
      start_date: Sequelize.STRING,
      end_date: Sequelize.STRING,
      capacity: Sequelize.INTEGER,
      description: Sequelize.STRING,
      status: {
        type: Sequelize.ENUM,
        values: ["enable", "disable", "unauthorized", "recused", "initialize"]
      }
    },
    { sequelize, modelName: "reservation" }
  );

  return Reservation;
};
