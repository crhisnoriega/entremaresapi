module.exports = function(Sequelize, sequelize, User) {
  class Account extends Sequelize.Model {}
  Account.init(
    {
      id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      type: { type: Sequelize.STRING, comment: "Account type - credit, debit" },
      creditCard: { type: Sequelize.STRING, comment: "Credit card number" },
      issuerCard: Sequelize.STRING,
      base64cv: {
        type: Sequelize.STRING,
        comment: "Credit card CVC in Base64 format"
      },
      base64dtexp: {
        type: Sequelize.STRING,
        comment: "Credit card Expire date in Base64 format"
      },
      accountId: Sequelize.STRING
    },
    { sequelize, modelName: "account" }
  );

  User.hasMany(Account);

  return Account;
};
