module.exports = function(Sequelize, sequelize, User) {
  class Waitress extends User {}
  Waitress.init(
    {
      id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      firstName: Sequelize.STRING,
      lastName: Sequelize.STRING,
      password: Sequelize.STRING,
      token: Sequelize.STRING,
      tokenDate: Sequelize.DATE,
      email: Sequelize.STRING
    },
    { sequelize, modelName: "waitress" }
  );

  return Waitress;
};
