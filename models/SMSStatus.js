
module.exports = function(Sequelize, sequelize) {
  class SMSStatus extends Sequelize.Model {}
  SMSStatus.init(
    {
      id: {
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV1
      },
      phone: Sequelize.STRING,
      smsCode: Sequelize.STRING,
      status: Sequelize.STRING,
      login: Sequelize.STRING,
      date: Sequelize.DATE
    },
    { sequelize, modelName: "smsstatus" }
  );

  return SMSStatus;
};
