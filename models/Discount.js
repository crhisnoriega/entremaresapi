module.exports = function(Sequelize, sequelize, Product) {
  class Discount extends Sequelize.Model {}
  Discount.init(
    {
      id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      discountAmount: Sequelize.DOUBLE,
      discountDeadline: Sequelize.DATE,
      status: {
        type: Sequelize.ENUM,
        values: ["enabled", "disable"]
      }
    },
    { sequelize, modelName: "discount" }
  );

  Product.hasMany(Discount);

  return Discount;
};
