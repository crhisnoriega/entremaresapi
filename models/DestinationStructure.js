module.exports = function(Sequelize, sequelize) {
  class DestinationStructure extends Sequelize.Model {}
  DestinationStructure.init({
    id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    destionation_id: Sequelize.STRING,
    subcategorty_id: Sequelize.STRING,
    beach_description: {
      type: Sequelize.TEXT('long'),
      get: function() {
        try {
          return JSON.parse(this.getDataValue("subcategory"));
        }
        catch (e) {}
      },
      set: function(val) {
        try {
          return this.setDataValue("subcategory", JSON.stringify(val));
        }
        catch (e) {}
      }
    },

  }, { sequelize, modelName: "category" });


  return DestinationStructure;
};
