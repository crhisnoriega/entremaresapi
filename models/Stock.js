module.exports = function(Sequelize, sequelize, User, StockItem) {
  class Stock extends Sequelize.Model {}
  Stock.init(
    {
      id: {
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV1
      },
      totalValue: Sequelize.DOUBLE,
      status: {
        type: Sequelize.ENUM,
        values: ["disable", "negative"]
      },
      dateStatus: Sequelize.DATE
    },
    { sequelize, modelName: "stock" }
  );

  User.hasOne(Stock);
  Stock.hasMany(StockItem);

  return Stock;
};
