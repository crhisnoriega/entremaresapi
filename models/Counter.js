module.exports = function(Sequelize, sequelize) {
  class Counter extends Sequelize.Model {}
  Counter.init(
    {
      id: {
        primaryKey: true,
        type: Sequelize.STRING
      },
      counter: Sequelize.INTEGER,
 
    },
    { sequelize, modelName: "counter" }
  );

  return Counter;
};