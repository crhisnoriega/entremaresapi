module.exports = function(Sequelize, sequelize, Establishment) {
  class Category extends Sequelize.Model {}
  Category.init(
    {
      id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      name: { type: Sequelize.STRING, comment: "Category name" },
      shortname: { type: Sequelize.STRING, comment: "Short category name" },
      description: Sequelize.STRING
    },
    { sequelize, modelName: "category" }
  );

  Category.belongsTo(Establishment);

  return Category;
};
