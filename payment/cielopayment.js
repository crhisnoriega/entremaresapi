const paramsCielo = {
  MerchantId: "53ade867-be99-4b64-a27c-47463410e81f",
  MerchantKey: "TMCZFLNGHJOWOLGZHVASGSNYNCGBMXXFWPXYAZSC",
  sandbox: true,
  debug: false
};
const cielo = require("cielo")(paramsCielo);

doPurchase = async (orderid, creditcard, amount, payment) => {
  var dadosSale = {
    MerchantOrderId: orderid,
    Customer: {
      Name: "Comprador crédito simples"
    },
    Payment: {
      Type: "CreditCard",
      Amount: amount,
      Installments: 1,
      SoftDescriptor: "123456789ABCD",
      CreditCard: {
        CardNumber: "0000000000000001",
        Holder: creditcard.holder,
        ExpirationDate: "12/2030",
        SecurityCode: "123",
        Brand: "Visa"
      }
    }
  };

  console.log(dadosSale);

  try {
    const transaction = await cielo.creditCard.simpleTransaction(dadosSale);
    payment.cieloTransaction = JSON.stringify(transaction);

    if ("Operation Successful" == transaction.Payment.ReturnMessage) {
      payment.cieloPaymentId = transaction.Payment.PaymentId;
      payment.status = "authorized";
    } else {
      payment.status = "unauthorized";
    }

    payment.save();
   
    return payment;
  } catch (e) {
    console.log(e);
    return null;
  }
};

cancelPurchase = async (paymentId, amount, payment) => {
  var dadosSale = {
    paymentId: paymentId,
    amount: amount
  };
  const cancel = await cielo.creditCard.cancelSale(dadosSale);

  payment.cieloTransaction = JSON.stringify(cancel);
  payment.status = "canceled";
  payment.save();
};

module.exports = { doPurchase, cancelPurchase };
