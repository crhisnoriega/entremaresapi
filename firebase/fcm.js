var firebase = require("firebase");
var admin = require("firebase-admin");

var config = {
  apiKey: "AIzaSyD6g370Kmp-3PLY7TdIAKM3JtcB3lKjJdQ",
  authDomain: "kiwkiwapp-6aadc.firebaseapp.com",
  databaseURL: "https://kiwkiwapp-6aadc.firebaseio.com",
  projectId: "kiwkiwapp-6aadc",
  storageBucket: "kiwkiwapp-6aadc.appspot.com",
  messagingSenderId: "422017066928"
};
firebase.initializeApp(config);

var serviceAccount = require("./kiwkiwapp-6aadc-firebase-adminsdk-6k9z5-5414c6c461.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://kiwkiwapp-6aadc.firebaseio.com"
});

sendFCMMessage = (fcmToken, message) => {
  var message = {
    data: message,
    token: fcmToken
  };

  firebase
    .messaging()
    .send(message)
    .then(response => {
      console.log("Successfully sent message:", response);
    })
    .catch(error => {
      console.log("Error sending message:", error);
    });
};

sendAdminMessage = (fcmToken, message) => {
  var payload = {
    data: message
  };

  var options = {
    priority: "normal",
    timeToLive: 60 * 60
  };

  admin
    .messaging()
    .sendToDevice(registrationToken, payload, options)
    .then(function(response) {
      console.log("Successfully sent message:", response);
    })
    .catch(function(error) {
      console.log("Error sending message:", error);
    });
};

module.exports = { sendFCMMessage, sendAdminMessage };
