const fs = require("fs");

const Joi = require("joi");
const { Order, OrderItem, User } = require("../persistence/db");
const uploadS3FileSync = require("../aws/s3");
const { sendFCMMessage } = require("../firebase/fcm");

// const redis = require("../aws/elasticache");

var itensSchema = Joi.object().keys({
  productId: Joi.string()
    .required()
    .description("username"),
  name: Joi.string()
    .required()
    .description("username"),
  unit: Joi.number()
    .required()
    .description("username"),
  unitPrice: Joi.number()
    .required()
    .description("username"),
  quantity: Joi.number()
    .required()
    .description("username"),
  totalPrice: Joi.number()
    .required()
    .description("username")
});

var userSchema = Joi.object().keys({
  userId: Joi.string()
    .required()
    .description("username")
});

module.exports = [
  {
    path: "/orders",
    method: "POST",
    config: {
      handler: async function(req, res) {
        console.log(req.payload.users);
        console.log(req.payload.itens);

        req.payload.status = "new";
        var order = await Order.create(req.payload);

        var items = req.payload.itens;
        for (item in items) {
          // var item = await OrderItem.create(req.payload.itens[0]);
          var item = await OrderItem.create(item);
          item.setOrder(order);
        }

        for (userid in req.payload.users) {
          var user = await User.findOne({ where: { id: userid } });
          order.addUser(user);

          // if shared account, sent confirm order
          if (order.type == "shared") {
            sendFCMMessage(user.fcmToken, { confirmSharedOrder: order });
          }
        }

        // redis.set(order.id, JSON.stringify(order));

        return res.response({ status: "OK", orderid: order.id });
      },
      description: "Register order",
      notes: "Take orders",
      tags: ["api", "order"],
      validate: {
        options: {
          allowUnknown: true
        },
        payload: {
          ownerUser: Joi.string().required(),
          type: Joi.string().required(),
          date: Joi.date().required(),
          itens: Joi.array().items(itensSchema),
          users: Joi.array().items(userSchema)
        }
      }
    }
  },
  {
    path: "/orders/{orderid}",
    method: "PUT",
    config: {
      handler: async function(req, res) {
        var result = await Order.update(req.payload, {
          where: { id: req.params.orderid }
        });
        if (result[0] == 1) {
          return res.response({ status: "OK" });
        } else {
          return res.response({ status: "error" });
        }
      },
      description: "Update order status",
      notes: "Take orders",
      tags: ["api", "order"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          orderid: Joi.string().required()
        },
        payload: {
          status: Joi.string().required()
        }
      }
    }
  },
  {
    path: "/orders/accept",
    method: "PUT",
    config: {
      handler: async function(req, res) {
        var order = await Order.findOne({ where: { id: req.payload.orderid } });

        var currentUser = null;
        var users = order.getUsers();
        for (user in users) {
          if (user.id == req.payload.userid) {
            currentUser = user;
            break;
          }
        }

        if (currentUser != null) {
        }

        return res.response({ status: "OK" });
      },
      description: "Register order",
      notes: "Take orders",
      tags: ["api", "order"],
      validate: {
        options: {
          allowUnknown: true
        },

        payload: {
          userid: Joi.string().required(),
          orderid: Joi.string().required()
        }
      }
    }
  }
];
